package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class NameArray {
	/*
	 * Ejercicio 2: Crea una clase llamada NameArray que defina un array de 10
	 * Strings vacios y luego en un bucle solicite al usuario que introduzca cada
	 * elemento. Despues se muestran todos los elementos con otro bucle.
	 */
	private static Scanner sc;

	public static void ejercicio2() {
		String[] nombres = new String[10];
		sc = new Scanner(System.in);
		for (int i = 0; i < nombres.length; i++) {
			System.out.println("Introduzca un nombre para la posicion " + i + " del array:");
			nombres[i] = sc.nextLine();
		}
		for (int i = 0; i < nombres.length; i++) {
			System.out.println("El nombre: " + nombres[i] + " esta en la posicion " + i);
		}
	}
}
