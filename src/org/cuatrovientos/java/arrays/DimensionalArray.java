package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class DimensionalArray {
	/*
	 * Ejercicio 9: Crea una clase llamada DimensionalArray que defina un array
	 * bidimensional de 5x5 numeros enteros inicializados a 0. Posteriormente
	 * deberas solicitar al usuario que introduzca cada elemento
	 		Pos [0] [0] - Enter a number: 
			2
			Pos [0] [1] - Enter a number: 
			4
			Pos [0] [2] - Enter a number: 
			5
			Pos [0] [3] - Enter a number: 
			6
			Pos [0] [4] - Enter a number: 
			7
			Pos [1] [0] - Enter a number: 
			8
			Pos [1] [1] - Enter a number: 
			9
	* el resultado mostrara la media de cada una de las filas:
			La media de la fila 0 es: 4
			La media de la fila 1 es: 5
			La media de la fila 2 es: 4
			La media de la fila 3 es: 6
			La media de la fila 4 es: 16
	 */
	private static Scanner sc;

	public static void ejercicio10() {
		sc = new Scanner(System.in);
		int[][] numeros = new int[5][5];
		for (int i = 0; i < numeros.length; i++) {
			for (int j = 0; j < numeros.length; j++) {
				System.out.println("Pos [" + i + "] [" + j + "] - Enter a number: ");
				numeros[i][j] = sc.nextInt();
			}
		}
		for (int i = 0; i < numeros.length; i++) {
			int media = 0;
			for (int j = 0; j < numeros.length; j++) {
				media = media + numeros[i][j];
			}
			System.out.println("La media de la fila " + i + " es: " + media / numeros.length);
		}
	}
}
