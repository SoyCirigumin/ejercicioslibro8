package org.cuatrovientos.java.arrays;

import java.util.Random;

public class RandomArray {
	/*
	 * Ejercicio 8: Crea una clase llamada RandomArray que defina un array de 10
	 * elementos. Crea un bucle que inicialice los valores del array usando numeros
	 * aleatorios. Despues de eso crea otro bucle que si encuentra el numero 15 en
	 * algun elemento interrumpa el bucle y muestre la posicion en la que esta.
	 		Random value inserted in 0 : 17
	  		Random value inserted in 1 : 17
	  		Random value inserted in 2 : 7
	   		Random value inserted in 3 : 16
	    	Random value inserted in 4 : 18 
	    	Random value inserted in 5 : 20
	    	Random value inserted in 6 : 7
	    	Random value inserted in 7 : 0
	    	Random value inserted in 8 : 12
	    	Random value inserted in 9 : 15
	    	Number 15 was found at: 9

	 * Vamos a variar la version anterior para que el array sea de dos dimensiones y
	 * almacene 5x5 elementos. De la misma manera inicializaras todas las posiciones
	 * con numeros aleatorios e indicaras si el numero 15 ha sido encontrado en
	 * alguna posicion, en cuyo caso se interrumpira el bucle. El resultado sera:
			Random value inserted in [0] [0] 11
			Random value inserted in [0] [1] 15
			Random value inserted in [0] [2] 14
			Random value inserted in [0] [3] 25
			Random value inserted in [0] [4] 27
			Random value inserted in [1] [0] 28
			Random value inserted in [1] [1] 19
			Random value inserted in [1] [2] 8
			Random value inserted in [1] [3] 20
			Random value inserted in [1] [4] 7
			Random value inserted in [2] [0] 13
			Random value inserted in [2] [1] 25
			Random value inserted in [2] [2] 13
			Random value inserted in [2] [3] 2
			Random value inserted in [2] [4] 28
			Random value inserted in [3] [0] 14
			Random value inserted in [3] [1] 2
			Random value inserted in [3] [2] 14
			Random value inserted in [3] [3] 8
			Random value inserted in [3] [4] 28
			Random value inserted in [4] [0] 12
			Random value inserted in [4] [1] 5
			Random value inserted in [4] [2] 1
			Random value inserted in [4] [3] 2
			Random value inserted in [4] [4] 7
			Number 15 was found at posicion: [0][1]
	 */
	public static void ejercicio8() {
		int[] numeros = new int[10];
		Random rnd = new Random();
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = rnd.nextInt(30);
			System.out.println("Random value inserted in " + i + " : " + numeros[i]);
		}
		boolean esta = false;
		int i = 0;
		while (!esta && i < numeros.length) {
			if (numeros[i] == 15) {
				esta = true;
			}
			i++;
		}
		if (esta) {
			System.out.println("Number 15 was found at: " + (i - 1));
		} else {
			System.out.println("Number 15 not was found");
		}

		// Segunda parte del ejercicio
		int[][] numeros1 = new int[5][5];
		for (i = 0; i < numeros1.length; i++) {
			for (int j = 0; j < numeros1.length; j++) {
				numeros1[i][j] = rnd.nextInt(30);
				System.out.println("Random value inserted in [" + i + "] [" + j + "] " + numeros1[i][j]);
			}
		}
		esta = false;
		i = 0;
		int j = 0;
		while (!esta && i < numeros1.length) {
			j = 0;
			while (!esta && j < numeros1.length) {
				if (numeros1[i][j] == 15) {
					esta = true;
				}
				j++;
			}
			i++;
		}
		if (esta) {
			System.out.println("Number 15 was found at posicion: [" + (i - 1) + "][" + (j - 1) + "]");
		} else {
			System.out.println("Number 15 not was found");
		}
	}

}
