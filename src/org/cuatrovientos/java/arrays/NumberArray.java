package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class NumberArray {
	/*
	 * Ejercicio 3: Crea una clase llamada NumberArray que defina un array de 10
	 * numeros enteros inicializados a 0 y luego en un bucle solicite al usuario que
	 * introduzca cada elemento. Luego crea otro bucle para determinar si en el
	 * array hay algun elemento repetido. Con que encuentre uno repetido es
	 * suficiente.
	 */

	private static Scanner sc;

	public static void ejercicio3() {
		int[] numeros = new int[10];
		sc = new Scanner(System.in);
		boolean repe = false;
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Introduzca un numero para la posicion " + i + " del array:");
			numeros[i] = sc.nextInt();
		}
		int i = 0;
		int j;
		do {
			j = i + 1;
			while (!repe && j < numeros.length) {
				if (numeros[i] == numeros[j]) {
					repe = true;
				}
				j++;
			}
			i++;
		} while (!repe && i < numeros.length);
		if (repe) {
			System.out.println("El numero: " + numeros[i - 1] + " de la posicion " + (i - 1)
					+ " es igual al de la posicion " + (j - 1));
		} else {
			System.out.println("No existe ningun numero repetido en el Array");
		}
	}
}
