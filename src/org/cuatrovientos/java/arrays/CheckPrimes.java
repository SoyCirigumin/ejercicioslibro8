package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class CheckPrimes {
	/*
	 * Ejercicio 7: Crea una clase llamada CheckPrimes que defina un array de 10
	 * numeros enteros inicializados a 0 y luego en un bucle solicite al usuario que
	 * introduzca cada elemento. Luego crea otro bucle que descubra que numeros son
	 * primos.
	 */
	private static Scanner sc;

	public static void ejercicio7() {
		int[] numeros = new int[10];
		sc = new Scanner(System.in);
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Introduzca un numero para la posicion " + i + " del array:");
			numeros[i] = sc.nextInt();
		}
		for (int i = 0; i < numeros.length; i++) {
			if (esPrimo(numeros[i])) {
				System.out.println("El numero " + numeros[i] + " de la posicion " + i + " es un numero primo");
			} else {
				System.out.println("El numero " + numeros[i] + " de la posicion " + i + " no es un numero primo");
			}
		}
	}

	public static boolean esPrimo(int x) {
		int contador = 2;
		boolean primo = true;
		if (x % 2 == 0 || x == 1) {
			primo = false;
		}
		while ((primo) && (contador != x)) {
			if (x % contador == 0)
				primo = false;
			contador++;
		}
		if (primo || x == 2) {
			return true;
		} else {
			return false;
		}
	}
}
