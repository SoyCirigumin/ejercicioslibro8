package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class AverageArray {
	/*
	 * Ejercicio 5: Crea una clase llamada AverageArray que defina un array de 10
	 * numeros con decimales (float) inicializados a 0 y luego en un bucle solicite
	 * al usuario que introduzca cada elemento. Luego crea otro bucle que calcule la
	 * media de todos los elementos.
	 */
	private static Scanner sc;

	public static void ejercicio5() {
		float[] numeros = new float[10];
		float resultado = 0;
		sc = new Scanner(System.in);
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Introduzca un numero para la posicion " + i + " del array:");
			numeros[i] = sc.nextFloat();
		}
		for (int i = 0; i < numeros.length; i++) {
			resultado = resultado + numeros[i];
		}
		System.out.println("La media de todos lo elementos del array es: " + resultado / numeros.length + 1);
	}
}
