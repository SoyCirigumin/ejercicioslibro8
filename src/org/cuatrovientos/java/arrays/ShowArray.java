package org.cuatrovientos.java.arrays;

public class ShowArray {

	public static void ejercicio1() {
		/*
		 * Ejercicio 1: Crea una clase llamada ShowArray que defina un array de 10
		 * numeros enteros y luego muestre cada elemento en un bucle
		 */
		int[] numeros = { 12, 23, 34, 45, 56, 67, 78, 89, 91, 102 };
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Numero: " + numeros[i] + " en la posicion " + i + " del Array");
		}
	}
}
