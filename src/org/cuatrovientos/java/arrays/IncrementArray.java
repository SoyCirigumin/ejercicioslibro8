package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class IncrementArray {
	/*
	 * Ejercicio 4: Crea una clase llamada IncrementArray que defina un array de 10
	 * numeros enteros inicializados a 0 y luego en un bucle solicite al usuario que
	 * introduzca cada elemento. Luego crea otro bucle para incrementar en uno cada
	 * elemento uno de los elementos y los muestre.
	 */
	private static Scanner sc;

	public static void ejercicio4() {
		int[] numeros = new int[10];
		sc = new Scanner(System.in);
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Introduzca un numero para la posicion " + i + " del array:");
			numeros[i] = sc.nextInt();
		}
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = numeros[i] + 1;
			System.out.println("El numero incrementado para la posicion " + i + " del array: " + numeros[i]);
		}
	}
}
