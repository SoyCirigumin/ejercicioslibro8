package org.cuatrovientos.java.arrays;

public class Ejecucion {

	public static void main(String[] args) {
		System.out.println("Ejercicio1:");
		ShowArray.ejercicio1();
		System.out.println("Ejercicio2:");
		NameArray.ejercicio2();
		System.out.println("Ejercicio3:");
		NumberArray.ejercicio3();
		System.out.println("Ejercicio4:");
		IncrementArray.ejercicio4();
		System.out.println("Ejercicio5:");
		AverageArray.ejercicio5();
		System.out.println("Ejercicio6:");
		CountArray.ejercicio6();
		System.out.println("Ejercicio7:");
		CheckPrimes.ejercicio7();
		System.out.println("Ejercicio8:");
		RandomArray.ejercicio8();
		System.out.println("Ejercicio9:");
		ShuffleArray.ejercicio9();
		System.out.println("Ejercicio10:");
		DimensionalArray.ejercicio10();
		System.out.println("Ejercicio11:");
		Menu.ejercicio11();
		
	}

}
