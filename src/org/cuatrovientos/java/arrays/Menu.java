package org.cuatrovientos.java.arrays;

import java.util.Random;
import java.util.Scanner;

public class Menu {

	/*
	 * Ejercicio 11: Crea una clase llamada Menu que dtenga las siguientres
	 * opciones: 
	 * Introduzca la opcion:
	 *  1. Generar Array 
	 *  2. Buscar numero 
	 *  3. Borrar numero 
	 *  4. Salir 
	 * Generar array: Se genera un array de dimension 5 con numeros aleatorios de entre 0 y 10. 
	 * Buscar numero: Se pide un numero por consola y se indica si se encuentra o no en el array anterior. 
	 * Borrar numero: Se pide un numero por consola y se sustituye en todas las posiciones en las que se
	 * encuentre por el numero 0. En caso de que no se encuentre, debera mostrar un mensaje indicandolo. 
	 * Salir: Finaliza el programa
	 */
	private static Scanner sc;

	public static void ejercicio11() {
		sc = new Scanner(System.in);
		Random rnd = new Random();
		int[] numeros = new int[5];
		int opc = 0;
		boolean esta = false;
		while (opc != 4) {
			System.out.println("Introduzca la opcion:");
			System.out.println("1. Generar Array");
			System.out.println("2. Buscar numero");
			System.out.println("3. Borrar numero");
			System.out.println("4. Salir");
			opc = sc.nextInt();
			switch (opc) {
			case 1: {
				for (int i = 0; i < numeros.length; i++) {
					numeros[i] = rnd.nextInt(10);
				}
				esta = true;
				showArray(numeros);
				break;
			}
			case 2: {
				if (esta) {
					int contador = 0;
					int x;
					System.out.println("Introduzca un numero para buscarlo en el array:");
					x = sc.nextInt();
					for (int i = 0; i < numeros.length; i++) {
						if (numeros[i] == x) {
							contador++;
						}
					}
					showArray(numeros);
					System.out.println("El numero: " + x + " se encontro: " + contador + " veces");
				} else {
					System.out.println("El array no esta generado. Pulse antes la opcion 1");
				}
				break;
			}
			case 3: {
				if (esta) {
					int contador = 0;
					int x;
					System.out.println("Introduzca un numero para sustituirlo en el array:");
					x = sc.nextInt();
					String posicion = "";
					for (int i = 0; i < numeros.length; i++) {
						if (numeros[i] == x) {
							numeros[i] = 0;
							posicion = posicion + " " + i;
							contador++;
						}
					}
					showArray(numeros);
					if (contador > 0) {
						System.out.println("El numero: " + x + " se cambio por 0: " + contador
								+ " veces en las pociciones: " + posicion);
					} else {
						System.out.println("El numero: " + x + " no se encontro en el array");
					}
				} else {
					System.out.println("El array no esta generado. Pulse antes la opcion 1");
				}
				break;
			}
			}
		}
		esta=false;
		do {
			System.out.println("Introduzca la opcion:");
			System.out.println("1. Generar Array");
			System.out.println("2. Buscar numero");
			System.out.println("3. Borrar numero");
			System.out.println("4. Salir");
			opc = sc.nextInt();
			switch (opc) {
			case 1: {
				for (int i = 0; i < numeros.length; i++) {
					numeros[i] = rnd.nextInt(10);
				}
				esta = true;
				showArray(numeros);
				break;
			}
			case 2: {
				if (esta) {
					int contador = 0;
					int x;
					System.out.println("Introduzca un numero para buscarlo en el array:");
					x = sc.nextInt();
					for (int i = 0; i < numeros.length; i++) {
						if (numeros[i] == x) {
							contador++;
						}
					}
					showArray(numeros);
					System.out.println("El numero: " + x + " se encontro: " + contador + " veces");
				} else {
					System.out.println("El array no esta generado. Pulse antes la opcion 1");
				}
				break;
			}
			case 3: {
				if (esta) {
					int contador = 0;
					int x;
					System.out.println("Introduzca un numero para sustituirlo en el array:");
					x = sc.nextInt();
					String posicion = "";
					for (int i = 0; i < numeros.length; i++) {
						if (numeros[i] == x) {
							numeros[i] = 0;
							posicion = posicion + " " + i;
							contador++;
						}
					}
					showArray(numeros);
					if (contador > 0) {
						System.out.println("El numero: " + x + " se cambio por 0: " + contador
								+ " veces en las pociciones: " + posicion);
					} else {
						System.out.println("El numero: " + x + " no se encontro en el array");
					}
				} else {
					System.out.println("El array no esta generado. Pulse antes la opcion 1");
				}
				break;
			}
			}
		} while (opc != 4);
	}

	private static void showArray(int[] numeros) {
		String array = "";
		for (int i = 0; i < numeros.length; i++) {
			array = array + numeros[i];
		}
		System.out.println("El array : " + array);
	}
}
