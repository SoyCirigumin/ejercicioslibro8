package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class CountArray {
	/*
	 * Ejercicio 6: Crea una clase llamada CountArray que defina un array de 10
	 * numeros enteros inicializados a 0 y luego en un bucle solicite al usuario que
	 * introduzca cada elemento. Luego crea otro bucle que contabilice el total de
	 * numeros positivos, negativos y los que sean 0.
	 */
	private static Scanner sc;

	public static void ejercicio6() {
		int[] numeros = new int[10];
		int pos = 0, neg = 0, cero = 0;
		sc = new Scanner(System.in);
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Introduzca un numero para la posicion " + i + " del array:");
			numeros[i] = sc.nextInt();
		}
		for (int i = 0; i < numeros.length; i++) {
			if (numeros[i] > 0) {
				pos++;
			} else if (numeros[i] < 0) {
				neg++;
			} else {
				cero++;
			}
		}
		System.out.println(
				"En el array existen: " + pos + " numeros positivos " + neg + " numeros negativos " + cero + " ceros");
	}
}
