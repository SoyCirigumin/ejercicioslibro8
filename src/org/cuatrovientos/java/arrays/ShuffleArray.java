package org.cuatrovientos.java.arrays;

import java.util.Random;
import java.util.Scanner;

public class ShuffleArray {
	/*
	 * Ejercicio 9: Crea una clase llamada ShuffleArray que defina un array de 10
	 * numeros enteros inicializados a 0 y luego en un bucle solicite al usuario que
	 * introduzca cada elemento. En un bucle muestra por pantalla todos los
	 * elementos. Luego crea otro bucle que baraje los elementos usando el metodo
	 * nextInt del ejercicio 8 en los indices (es decir, aleatoriamente saca un
	 * indice del array, y luego otro. Intercambia el contenido del array para ese
	 * indice con el del otro).Finalmente muestra el resultado.
	  		Generated values:
			0:   2
			1:   3
			2:   4
			3:   5
			4:   2
			5:   3
			6:   4
			7:   7
			8:   8
			9:   6
			After shuffle:
			0:   3
			1:   8
			2:   6
			3:   7
			4:   5
			5:   2
			6:   4
			7:   3
			8:   4
			9:   2
	 */
	private static Scanner sc;

	public static void ejercicio9() {
		int[] numeros = new int[10];
		sc = new Scanner(System.in);
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Introduzca un numero para la posicion " + i + " del array:");
			numeros[i] = sc.nextInt();
		}
		System.out.println("Generated values:");
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(i + ":   " + numeros[i]);
		}
		Random rnd = new Random();
		for (int i = 0; i < numeros.length; i++) {
			int x = numeros[i];
			int j = rnd.nextInt(4);
			numeros[i] = numeros[j];
			numeros[j] = x;
		}
		System.out.println("After shuffle:");
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(i + ":   " + numeros[i]);
		}

	}
}
